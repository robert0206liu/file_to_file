#include <stdio.h>
#include <stdlib.h>

#define DEST_FILE "dest.txt"
#define SOUR_FILE "sour.txt"

void FileToFile(char* dest_file, char* sour_file)
{
    // LOCAL VARIABLES DECLARATION
    FILE *f_dest_file = NULL;
    FILE *f_sour_file = NULL;
    char *buf;
    
    // BODY
    buf = (char*)malloc(sizeof(char)*50);
    
    f_dest_file = fopen(dest_file, "a");
    f_sour_file = fopen(sour_file, "r");
    
    while(fgets(buf, 50, f_sour_file) != NULL)
    {
        fputs(buf, f_dest_file);
    }
    
    free(buf);
    fclose(f_dest_file);
    fclose(f_sour_file);
}

int main()
{
    FileToFile(DEST_FILE, SOUR_FILE);
    return 0;
}